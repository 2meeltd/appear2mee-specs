#
# Be sure to run `pod lib lint Appear2meeSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Appear2meeSDK'
  s.version          = '1.1.0'
  s.summary          = '2mee notification SDK.'


  s.description      = <<-DESC
The SDK (as an xcframework) to allow 2mee message notifications to be downloaded and displayed. Use in App, Service Extension and Content Extension
                       DESC

  s.homepage         = 'https://2mee.com/'
  s.license          = "Copyright 2mee Ltd"
#  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '2mee Ltd' => 'Gerard.Allan@2mee.com' }
  s.source           = { :git => 'https://bitbucket.com/2meeltd/appear2meesdk.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
  s.vendored_frameworks = 'Appear2meeFramework/Appear2mee.xcframework'

end
